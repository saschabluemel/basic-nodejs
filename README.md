## Node.js 'Hello World' auf Heroku von Bitbucket aus deployen *Walkthrough*

1. Account auf [Heroku](https://heroku.com) erstellen
2. Account auf [Bitbucket](https://bitbucket.org) erstellen
3. Repo erstellen auf Bitbucket. Beim erstellen auf Import klicken und die URL von diesem Repo nehmen
4. Pipeline in den Bitbucket Settings des Repos aktivieren
5. Neue App in Heroku anlegen. Der Name ist beliebig, keine weitere Konfiguration nötig.
6. HEROKU_API_KEY (API Key aus den Heroku Account Settings) und HEROKU_APP_NAME (Name der vorher angelegten App) als  Enviornment Variables in den Repo Settings anlegen
7. Build antriggern - das deployt + startet die App auf Heroku und das wars :-)

## Info zum Deployment und Build

[Info zum Deployment von Bitbucket zu Heroku](https://confluence.atlassian.com/bitbucket/deploy-to-heroku-872013667.html)

[Build-Pipelines auf Bitbucket](https://confluence.atlassian.com/bitbucket/language-guides-856821477.html)

## Image-Support von Bitbucket für Node.js:

<https://hub.docker.com/_/node/>

## Buildpacks für Heroku:

<https://devcenter.heroku.com/articles/buildpacks>

## Weiterführende Links

- [Getting Started with Node.js on Heroku](https://devcenter.heroku.com/articles/getting-started-with-nodejs)
- [Heroku Node.js Support](https://devcenter.heroku.com/articles/nodejs-support)
- [Node.js on Heroku](https://devcenter.heroku.com/categories/nodejs)
- [Best Practices for Node.js Development](https://devcenter.heroku.com/articles/node-best-practices)
- [Using WebSockets on Heroku with Node.js](https://devcenter.heroku.com/articles/node-websockets)
- [package.json structure](https://docs.npmjs.com/files/package.json)
